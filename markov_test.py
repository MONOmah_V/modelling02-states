__author__ = 'monomah'

import unittest
import numpy
import numpy.testing

from markov import getSystemTimes

class MarkovTestCase1(unittest.TestCase):
    def setUp(self):
        self.coefficientMatrix = [[0, 1, 2, 0],
                                  [2, 0, 0, 2],
                                  [3, 0, 0, 1],
                                  [0, 3, 2, 0]]
        self.result = numpy.array([0.400, 0.200, 0.267, 0.133])

    def test_getSystemTimes(self):
        test = getSystemTimes(self.coefficientMatrix)
        numpy.testing.assert_array_almost_equal(test, self.result, decimal=3)

if __name__ == '__main__':
    unittest.main()
